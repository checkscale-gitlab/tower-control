.DEFAULT_GOAL := help

help:
	@echo "${PROJECT}"
	@echo "${DESCRIPTION}"
	@echo ""
	@echo "	venv - create venv"
	@echo "	jbridge - launch jbridge web server"

################ Project #######################
PROJECT ?= tower-control
DESCRIPTION ?= Monitor Home Automation Systems
################################################

################ Config ########################
S3_BUCKET ?=
ARTIFACTS_BUCKET ?=
AWS_REGION ?= eu-west-1
ENV ?= dev
ECR ?=
################################################

venv:
	@pip3 install -r ./scripts/requirements.txt


jbridge:
	@python3 ./scripts/jbridge/bridge-jeedom.py

volume-size:
	@docker system df --verbose|grep "VOLUME NAME" -A 4

