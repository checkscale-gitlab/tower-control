# Telegraf Configuration
#
# Telegraf is entirely plugin driven. All metrics are gathered from the
# declared inputs, and sent to the declared outputs.
#
# Plugins must be declared in here to be active.
# To deactivate a plugin, comment out the name and any variables.
#
# Use 'telegraf -config telegraf.conf -test' to see what metrics a config
# file would generate.
#
# Environment variables can be used anywhere in this config file, simply surround
# them with ${}. For strings the variable must be within quotes (ie, "${STR_VAR}"),
# for numbers and booleans they should be plain (ie, ${INT_VAR}, ${BOOL_VAR})

# Global tags can be specified here in key="value" format.
[global_tags]
  # dc = "us-east-1" # will tag all metrics with dc=us-east-1
  # rack = "1a"
  ## Environment variables can be used as tags, and throughout the config file
  # user = "$USER"

# Configuration for telegraf agent
[agent]
  ## Default data collection interval for all inputs
  interval = "10s"
  ## Rounds collection interval to 'interval'
  ## ie, if interval="10s" then always collect on :00, :10, :20, etc.
  round_interval = true

  ## Telegraf will send metrics to outputs in batches of at most
  ## metric_batch_size metrics.
  ## This controls the size of writes that Telegraf sends to output plugins.
  metric_batch_size = 1000

  ## Maximum number of unwritten metrics per output.  Increasing this value
  ## allows for longer periods of output downtime without dropping metrics at the
  ## cost of higher maximum memory usage.
  metric_buffer_limit = 10000

  ## Collection jitter is used to jitter the collection by a random amount.
  ## Each plugin will sleep for a random time within jitter before collecting.
  ## This can be used to avoid many plugins querying things like sysfs at the
  ## same time, which can have a measurable effect on the system.
  collection_jitter = "0s"

  ## Default flushing interval for all outputs. Maximum flush_interval will be
  ## flush_interval + flush_jitter
  flush_interval = "10s"
  ## Jitter the flush interval by a random amount. This is primarily to avoid
  ## large write spikes for users running a large number of telegraf instances.
  ## ie, a jitter of 5s and interval 10s means flushes will happen every 10-15s
  flush_jitter = "0s"

  ## By default or when set to "0s", precision will be set to the same
  ## timestamp order as the collection interval, with the maximum being 1s.
  ##   ie, when interval = "10s", precision will be "1s"
  ##       when interval = "250ms", precision will be "1ms"
  ## Precision will NOT be used for service inputs. It is up to each individual
  ## service input to set the timestamp at the appropriate precision.
  ## Valid time units are "ns", "us" (or "µs"), "ms", "s".
  precision = ""

  ## Log at debug level.
  # debug = false
  ## Log only error level messages.
  # quiet = false

  ## Log target controls the destination for logs and can be one of "file",
  ## "stderr" or, on Windows, "eventlog".  When set to "file", the output file
  ## is determined by the "logfile" setting.
  # logtarget = "file"

  ## Name of the file to be logged to when using the "file" logtarget.  If set to
  ## the empty string then logs are written to stderr.
  # logfile = ""

  ## The logfile will be rotated after the time interval specified.  When set
  ## to 0 no time based rotation is performed.  Logs are rotated only when
  ## written to, if there is no log activity rotation may be delayed.
  # logfile_rotation_interval = "0d"

  ## The logfile will be rotated when it becomes larger than the specified
  ## size.  When set to 0 no size based rotation is performed.
  # logfile_rotation_max_size = "0MB"

  ## Maximum number of rotated archives to keep, any older logs are deleted.
  ## If set to -1, no archives are removed.
  # logfile_rotation_max_archives = 5

  ## Override default hostname, if empty use os.Hostname()
  hostname = ""
  ## If set to true, do no set the "host" tag in the telegraf agent.
  omit_hostname = false

###############################################################################
#                            OUTPUT PLUGINS                                   #
###############################################################################


# Configuration for sending metrics to InfluxDB
[[outputs.influxdb]]
  urls = ["http://127.0.0.1:8086"]
  database = "db0"

  ## Timeout for HTTP messages.
  # timeout = "5s"

  ## HTTP Basic Auth
  #username = "admin"
  #password = "admin"

  ## HTTP User-Agent
  # user_agent = "telegraf"

  ## UDP payload size is the maximum packet size to send.
  # udp_payload = "512B"

  ## Optional TLS Config for use on HTTP connections.
  # tls_ca = "/etc/telegraf/ca.pem"
  # tls_cert = "/etc/telegraf/cert.pem"
  # tls_key = "/etc/telegraf/key.pem"
  ## Use TLS but skip chain & host verification
  # insecure_skip_verify = false

  ## HTTP Proxy override, if unset values the standard proxy environment
  ## variables are consulted to determine which proxy, if any, should be used.
  # http_proxy = "http://corporate.proxy:3128"

  ## Additional HTTP headers
  # http_headers = {"X-Special-Header" = "Special-Value"}

  ## HTTP Content-Encoding for write request body, can be set to "gzip" to
  ## compress body or "identity" to apply no encoding.
  # content_encoding = "identity"

  ## When true, Telegraf will output unsigned integers as unsigned values,
  ## i.e.: "42u".  You will need a version of InfluxDB supporting unsigned
  ## integer values.  Enabling this option will result in field type errors if
  ## existing data has been written.
  # influx_uint_support = false

###############################################################################
#                            INPUT PLUGINS                                    #
###############################################################################

[[inputs.cpu]]
    percpu = true
    totalcpu = true
    collect_cpu_time = false
    report_active = false
[[inputs.disk]]
    ignore_fs = ["tmpfs", "devtmpfs", "devfs"]
[[inputs.io]]
[[inputs.mem]]
[[inputs.net]]
[[inputs.system]]
[[inputs.swap]]
[[inputs.netstat]]
[[inputs.processes]]
[[inputs.kernel]]

# # Pull Metric Statistics from Amazon CloudWatch
# [[inputs.cloudwatch]]
#   ## Amazon Region
#   region = "us-east-1"
#
#   ## Amazon Credentials
#   ## Credentials are loaded in the following order
#   ## 1) Assumed credentials via STS if role_arn is specified
#   ## 2) explicit credentials from 'access_key' and 'secret_key'
#   ## 3) shared profile from 'profile'
#   ## 4) environment variables
#   ## 5) shared credentials file
#   ## 6) EC2 Instance Profile
#   # access_key = ""
#   # secret_key = ""
#   # token = ""
#   # role_arn = ""
#   # profile = ""
#   # shared_credential_file = ""
#
#   ## Endpoint to make request against, the correct endpoint is automatically
#   ## determined and this option should only be set if you wish to override the
#   ## default.
#   ##   ex: endpoint_url = "http://localhost:8000"
#   # endpoint_url = ""
#
#   # The minimum period for Cloudwatch metrics is 1 minute (60s). However not all
#   # metrics are made available to the 1 minute period. Some are collected at
#   # 3 minute, 5 minute, or larger intervals. See https://aws.amazon.com/cloudwatch/faqs/#monitoring.
#   # Note that if a period is configured that is smaller than the minimum for a
#   # particular metric, that metric will not be returned by the Cloudwatch API
#   # and will not be collected by Telegraf.
#   #
#   ## Requested CloudWatch aggregation Period (required - must be a multiple of 60s)
#   period = "5m"
#
#   ## Collection Delay (required - must account for metrics availability via CloudWatch API)
#   delay = "5m"
#
#   ## Recommended: use metric 'interval' that is a multiple of 'period' to avoid
#   ## gaps or overlap in pulled data
#   interval = "5m"
#
#   ## Configure the TTL for the internal cache of metrics.
#   # cache_ttl = "1h"
#
#   ## Metric Statistic Namespace (required)
#   namespace = "AWS/ELB"
#
#   ## Maximum requests per second. Note that the global default AWS rate limit is
#   ## 50 reqs/sec, so if you define multiple namespaces, these should add up to a
#   ## maximum of 50.
#   ## See http://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/cloudwatch_limits.html
#   # ratelimit = 25
#
#   ## Timeout for http requests made by the cloudwatch client.
#   # timeout = "5s"
#
#   ## Namespace-wide statistic filters. These allow fewer queries to be made to
#   ## cloudwatch.
#   # statistic_include = [ "average", "sum", "minimum", "maximum", sample_count" ]
#   # statistic_exclude = []
#
#   ## Metrics to Pull
#   ## Defaults to all Metrics in Namespace if nothing is provided
#   ## Refreshes Namespace available metrics every 1h
#   #[[inputs.cloudwatch.metrics]]
#   #  names = ["Latency", "RequestCount"]
#   #
#   #  ## Statistic filters for Metric.  These allow for retrieving specific
#   #  ## statistics for an individual metric.
#   #  # statistic_include = [ "average", "sum", "minimum", "maximum", sample_count" ]
#   #  # statistic_exclude = []
#   #
#   #  ## Dimension filters for Metric.  All dimensions defined for the metric names
#   #  ## must be specified in order to retrieve the metric statistics.
#   #  [[inputs.cloudwatch.metrics.dimensions]]
#   #    name = "LoadBalancerName"
#   #    value = "p-example"


# # Read metrics about docker containers
[[inputs.docker]]
  endpoint = "unix:///var/run/docker.sock"

  gather_services = false

  container_name_include = []
  container_name_exclude = []

  timeout = "5s"

  docker_label_include = []
  docker_label_exclude = []

  perdevice = true

  total = false


# # Count files in a directory
# [[inputs.filecount]]
#   ## Directory to gather stats about.
#   ##   deprecated in 1.9; use the directories option
#   # directory = "/var/cache/apt/archives"
#
#   ## Directories to gather stats about.
#   ## This accept standard unit glob matching rules, but with the addition of
#   ## ** as a "super asterisk". ie:
#   ##   /var/log/**    -> recursively find all directories in /var/log and count files in each directories
#   ##   /var/log/*/*   -> find all directories with a parent dir in /var/log and count files in each directories
#   ##   /var/log       -> count all files in /var/log and all of its subdirectories
#   directories = ["/var/cache/apt/archives"]
#
#   ## Only count files that match the name pattern. Defaults to "*".
#   name = "*.deb"
#
#   ## Count files in subdirectories. Defaults to true.
#   recursive = false
#
#   ## Only count regular files. Defaults to true.
#   regular_only = true
#
#   ## Follow all symlinks while walking the directory tree. Defaults to false.
#   follow_symlinks = false
#
#   ## Only count files that are at least this size. If size is
#   ## a negative number, only count files that are smaller than the
#   ## absolute value of size. Acceptable units are B, KiB, MiB, KB, ...
#   ## Without quotes and units, interpreted as size in bytes.
#   size = "0B"
#
#   ## Only count files that have not been touched for at least this
#   ## duration. If mtime is negative, only count files that have been
#   ## touched in this duration. Defaults to "0s".
#   mtime = "0s"

# # Gather repository information from GitHub hosted repositories.
# [[inputs.github]]
#   ## List of repositories to monitor.
#   repositories = [
# 	  "influxdata/telegraf",
# 	  "influxdata/influxdb"
#   ]
#
#   ## Github API access token.  Unauthenticated requests are limited to 60 per hour.
#   # access_token = ""
#
#   ## Github API enterprise url. Github Enterprise accounts must specify their base url.
#   # enterprise_base_url = ""
#
#   ## Timeout for HTTP requests.
#   # http_timeout = "5s"

# # Monitor disks' temperatures using hddtemp
# [[inputs.hddtemp]]
#   ## By default, telegraf gathers temps data from all disks detected by the
#   ## hddtemp.
#   ##
#   ## Only collect temps from the selected disks.
#   ##
#   ## A * as the device name will return the temperature values of all disks.
#   ##
#   # address = "127.0.0.1:7634"
#   # devices = ["sda", "*"]


# # Read formatted metrics from one or more HTTP endpoints
# [[inputs.http]]
#   ## One or more URLs from which to read formatted metrics
#   urls = [
#     "http://localhost/metrics"
#   ]
#
#   ## HTTP method
#   # method = "GET"
#
#   ## Optional HTTP headers
#   # headers = {"X-Special-Header" = "Special-Value"}
#
#   ## Optional HTTP Basic Auth Credentials
#   # username = "username"
#   # password = "pa$$word"
#
#   ## HTTP entity-body to send with POST/PUT requests.
#   # body = ""
#
#   ## HTTP Content-Encoding for write request body, can be set to "gzip" to
#   ## compress body or "identity" to apply no encoding.
#   # content_encoding = "identity"
#
#   ## Optional TLS Config
#   # tls_ca = "/etc/telegraf/ca.pem"
#   # tls_cert = "/etc/telegraf/cert.pem"
#   # tls_key = "/etc/telegraf/key.pem"
#   ## Use TLS but skip chain & host verification
#   # insecure_skip_verify = false
#
#   ## Amount of time allowed to complete the HTTP request
#   # timeout = "5s"
#
#   ## List of success status codes
#   # success_status_codes = [200]
#
#   ## Data format to consume.
#   ## Each data format has its own unique set of configuration options, read
#   ## more about them here:
#   ## https://github.com/influxdata/telegraf/blob/master/docs/DATA_FORMATS_INPUT.md
#   # data_format = "influx"


# # HTTP/HTTPS request given an address a method and a timeout
# [[inputs.http_response]]
#   ## Deprecated in 1.12, use 'urls'
#   ## Server address (default http://localhost)
#   # address = "http://localhost"
#
#   ## List of urls to query.
#   # urls = ["http://localhost"]
#
#   ## Set http_proxy (telegraf uses the system wide proxy settings if it's is not set)
#   # http_proxy = "http://localhost:8888"
#
#   ## Set response_timeout (default 5 seconds)
#   # response_timeout = "5s"
#
#   ## HTTP Request Method
#   # method = "GET"
#
#   ## Whether to follow redirects from the server (defaults to false)
#   # follow_redirects = false
#
#   ## Optional HTTP Request Body
#   # body = '''
#   # {'fake':'data'}
#   # '''
#
#   ## Optional substring or regex match in body of the response
#   # response_string_match = "\"service_status\": \"up\""
#   # response_string_match = "ok"
#   # response_string_match = "\".*_status\".?:.?\"up\""
#
#   ## Optional TLS Config
#   # tls_ca = "/etc/telegraf/ca.pem"
#   # tls_cert = "/etc/telegraf/cert.pem"
#   # tls_key = "/etc/telegraf/key.pem"
#   ## Use TLS but skip chain & host verification
#   # insecure_skip_verify = false
#
#   ## HTTP Request Headers (all values must be strings)
#   # [inputs.http_response.headers]
#   #   Host = "github.com"
#
#   ## Interface to use when dialing an address
#   # interface = "eth0"

# # Read InfluxDB-formatted JSON metrics from one or more HTTP endpoints
# [[inputs.influxdb]]
#   ## Works with InfluxDB debug endpoints out of the box,
#   ## but other services can use this format too.
#   ## See the influxdb plugin's README for more details.
#
#   ## Multiple URLs from which to read InfluxDB-formatted JSON
#   ## Default is "http://localhost:8086/debug/vars".
#   urls = [
#     "http://localhost:8086/debug/vars"
#   ]
#
#   ## Username and password to send using HTTP Basic Authentication.
#   # username = ""
#   # password = ""
#
#   ## Optional TLS Config
#   # tls_ca = "/etc/telegraf/ca.pem"
#   # tls_cert = "/etc/telegraf/cert.pem"
#   # tls_key = "/etc/telegraf/key.pem"
#   ## Use TLS but skip chain & host verification
#   # insecure_skip_verify = false
#
#   ## http request & header timeout
#   timeout = "5s"

# # Read TCP metrics such as established, time wait and sockets counts.
# [[inputs.netstat]]
#   # no configuration

# # Read current weather and forecasts data from openweathermap.org
 [[inputs.openweathermap]]
#   ## OpenWeatherMap API key.
   app_id = "76b00999e7f2709d4c93387840f29a0d"
#
#   ## City ID's to collect weather data from.
   city_id = ["2992650"]
#
#   ## Language of the description field. Can be one of "ar", "bg",
#   ## "ca", "cz", "de", "el", "en", "fa", "fi", "fr", "gl", "hr", "hu",
#   ## "it", "ja", "kr", "la", "lt", "mk", "nl", "pl", "pt", "ro", "ru",
#   ## "se", "sk", "sl", "es", "tr", "ua", "vi", "zh_cn", "zh_tw"
   lang = "en"
#
#   ## APIs to fetch; can contain "weather" or "forecast".
   fetch = ["weather", "forecast"]
#
#   ## OpenWeatherMap base URL
   base_url = "https://api.openweathermap.org/"
#
#   ## Timeout for HTTP response.
   response_timeout = "5s"
#
#   ## Preferred unit system for temperature and wind speed. Can be one of
#   ## "metric", "imperial", or "standard".
   units = "metric"
#
#   ## Query interval; OpenWeatherMap updates their weather data every 10
#   ## minutes.
   interval = "10m"

# # Ping given url(s) and return statistics
 [[inputs.ping]]
#   ## Hosts to send ping packets to.
   urls = ["google.fr"]
#
#   ## Method used for sending pings, can be either "exec" or "native".  When set
#   ## to "exec" the systems ping command will be executed.  When set to "native"
#   ## the plugin will send pings directly.
#   ##
#   ## While the default is "exec" for backwards compatibility, new deployments
#   ## are encouraged to use the "native" method for improved compatibility and
#   ## performance.
    method = "native"
#
#   ## Number of ping packets to send per interval.  Corresponds to the "-c"
#   ## option of the ping command.
    count = 3
#
#   ## Time to wait between sending ping packets in seconds.  Operates like the
#   ## "-i" option of the ping command.
    ping_interval = 3.0
#
#   ## If set, the time to wait for a ping response in seconds.  Operates like
#   ## the "-W" option of the ping command.
    timeout = 3.0
#
#   ## If set, the total ping deadline, in seconds.  Operates like the -w option
#   ## of the ping command.
#   # deadline = 10
#
#   ## Interface or source address to send ping from.  Operates like the -I or -S
#   ## option of the ping command.
#   # interface = ""
#
#   ## Specify the ping executable binary.
#   # binary = "ping"
#
#   ## Arguments for ping command. When arguments is not empty, the command from
#   ## the binary option will be used and other options (ping_interval, timeout,
#   ## etc) will be ignored.
#   # arguments = ["-c", "3"]
#
#   ## Use only IPv6 addresses when resolving a hostname.
#   # ipv6 = false


# # Monitor process cpu and memory usage
# [[inputs.procstat]]
#   ## PID file to monitor process
#   pid_file = "/var/run/nginx.pid"
#   ## executable name (ie, pgrep <exe>)
#   # exe = "nginx"
#   ## pattern as argument for pgrep (ie, pgrep -f <pattern>)
#   # pattern = "nginx"
#   ## user as argument for pgrep (ie, pgrep -u <user>)
#   # user = "nginx"
#   ## Systemd unit name
#   # systemd_unit = "nginx.service"
#   ## CGroup name or path
#   # cgroup = "systemd/system.slice/nginx.service"
#
#   ## Windows service name
#   # win_service = ""
#
#   ## override for process_name
#   ## This is optional; default is sourced from /proc/<pid>/status
#   # process_name = "bar"
#
#   ## Field name prefix
#   # prefix = ""
#
#   ## When true add the full cmdline as a tag.
#   # cmdline_tag = false
#
#   ## Add PID as a tag instead of a field; useful to differentiate between
#   ## processes whose tags are otherwise the same.  Can create a large number
#   ## of series, use judiciously.
#   # pid_tag = false
#
#   ## Method to use when finding process IDs.  Can be one of 'pgrep', or
#   ## 'native'.  The pgrep finder calls the pgrep executable in the PATH while
#   ## the native finder performs the search directly in a manor dependent on the
#   ## platform.  Default is 'pgrep'
#   # pid_finder = "pgrep"


[[inputs.snmp]]
    agents = ["udp://192.168.1.254:161"]
    community = "public"
    version = 2
    timeout = 5.0
    retries = 2

## DDWRT
# eth0
 [[inputs.snmp.field]]
  name = "eth0OutOctets"
  oid = "1.3.6.1.2.1.2.2.1.16.2"
 [[inputs.snmp.field]]
  name = "eth0InOctets"
  oid = "1.3.6.1.2.1.2.2.1.10.2"

# br0 - WLAN
 [[inputs.snmp.field]]
  name = "br0OutOctets"
  oid = "1.3.6.1.2.1.2.2.1.16.8"
 [[inputs.snmp.field]]
  name = "br0InOctets"
  oid = "1.3.6.1.2.1.2.2.1.10.8"

# ath0 - WLAN
 [[inputs.snmp.field]]
  name = "ath0OutOctets"
  oid = "1.3.6.1.2.1.2.2.1.16.9"
 [[inputs.snmp.field]]
  name = "ath0InOctets"
  oid = "1.3.6.1.2.1.2.2.1.10.9"

# ath1 - WLAN
 [[inputs.snmp.field]]
  name = "ath1OutOctets"
  oid = "1.3.6.1.2.1.2.2.1.16.10"
 [[inputs.snmp.field]]
  name = "ath1InOctets"
  oid = "1.3.6.1.2.1.2.2.1.10.10"

# CPU - Load 5 min average
 [[inputs.snmp.field]]
  name = "CPULoad5AVG"
  oid = "1.3.6.1.4.1.2021.10.1.5.2"

# # Read metrics from storage devices supporting S.M.A.R.T.
# [[inputs.smart]]
#   ## Optionally specify the path to the smartctl executable
#   # path = "/usr/bin/smartctl"
#
#   ## On most platforms smartctl requires root access.
#   ## Setting 'use_sudo' to true will make use of sudo to run smartctl.
#   ## Sudo must be configured to to allow the telegraf user to run smartctl
#   ## without a password.
#   # use_sudo = false
#
#   ## Skip checking disks in this power mode. Defaults to
#   ## "standby" to not wake up disks that have stoped rotating.
#   ## See --nocheck in the man pages for smartctl.
#   ## smartctl version 5.41 and 5.42 have faulty detection of
#   ## power mode and might require changing this value to
#   ## "never" depending on your disks.
#   # nocheck = "standby"
#
#   ## Gather all returned S.M.A.R.T. attribute metrics and the detailed
#   ## information from each drive into the 'smart_attribute' measurement.
#   # attributes = false
#
#   ## Optionally specify devices to exclude from reporting.
#   # excludes = [ "/dev/pass6" ]
#
#   ## Optionally specify devices and device type, if unset
#   ## a scan (smartctl --scan) for S.M.A.R.T. devices will
#   ## done and all found will be included except for the
#   ## excluded in excludes.
#   # devices = [ "/dev/ada0 -d atacam" ]
#
#   ## Timeout for the smartctl command to complete.
#   # timeout = "30s"

[[inputs.temp]]
#   # no configuration

# # Monitor wifi signal strength and quality
# [[inputs.wireless]]
#   ## Sets 'proc' directory path
#   ## If not specified, then default is /proc
#   # host_proc = "/proc"
